package io.mohadkamil.resty.persistence.entity.repositories;

import io.mohadkamil.resty.persistence.entity.Book;
import org.springframework.data.repository.ListCrudRepository;

public interface BookRepository extends ListCrudRepository<Book,Long> {
}
