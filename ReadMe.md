Example response

```json
{
  "_embedded": {
    "books": [
      {
        "name": "Thinking, Fast and Slow",
        "_links": {
          "self": {
            "href": "http://localhost:8081/books/1"
          },
          "book": {
            "href": "http://localhost:8081/books/1"
          }
        }
      },
      {
        "name": "Effective Java",
        "_links": {
          "self": {
            "href": "http://localhost:8081/books/2"
          },
          "book": {
            "href": "http://localhost:8081/books/2"
          }
        }
      }
    ]
  },
  "_links": {
    "self": {
      "href": "http://localhost:8081/books"
    },
    "profile": {
      "href": "http://localhost:8081/profile/books"
    }
  }
}
```